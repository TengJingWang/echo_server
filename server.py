#!/usr/bin/env python

import threading
import socket
import sys
import logging

host = '0.0.0.0'
port = 50000
backlog = 5
size = 1024


class Workthread(threading.Thread):
    def __init__(self, client, address):
        threading.Thread.__init__(self)
        self.client=client
        self.address=address

    def run(self):
        while True:
            data = self.client.recv(size)
            if data:
                print("client "+ str(self.address) +" sent:" + data.decode("UTF-8"))
                sys.stdout.flush()
                self.client.send(data)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.info("Starting")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    logging.info("binding")
    s.bind((host, port))
    logging.info("listening")
    s.listen(backlog)

    while True:
        logging.info("accepting connection")
        client, address = s.accept()
        logging.info("connection accepted")
        newthread = Workthread(client, address)
        newthread.start()