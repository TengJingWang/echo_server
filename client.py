#/usr/bin/env python

import socket
import time

host = 'echo_server'
port = 50000
size = 1024



s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))

while True:
    message = str(time.asctime())
    print("Sending data: " + message)
    message = message.encode("UTF-8")
    s.send(message)
    data = s.recv(size)
    print("Received from server: " + data.decode("UTF-8"))
    time.sleep(1)